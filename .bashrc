# Git completion
[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# RVM
export PATH="$PATH:$HOME/.rvm/bin"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
[[ -r $rvm_path/scripts/completion ]] && . $rvm_path/scripts/completion

# Pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PATH:$PYENV_ROOT/bin:$PYENV_ROOT/shims"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Android
export ANDROID_HOME="$HOME/Library/Android/sdk"
export ANDROID_NDK_HOME="$ANDROID_HOME/ndk-bundle"
export PATH="$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/emulator"

# Elastic Beanstalk
export PATH="$PATH:$HOME/.aws-eb/eb/linux/python2.7/"

# Java
export JAVA_HOME=$(/usr/libexec/java_home)

# Set neovim as editor
export VISUAL=nvim
export EDITOR="$VISUAL"

# PS1 colors
RESET=$(tput sgr0)
FG_BRIGHT_GREEN=$(tput setaf 10)
FG_BRIGHT_YELLOW=$(tput setaf 11)

# Git info for prompt
function git_info () {
  git rev-parse --is-inside-work-tree &>/dev/null || return
  branch=$(git symbolic-ref -q HEAD | sed -e 's|^refs/heads/||')
  dirty=$(git diff --quiet --ignore-submodules HEAD &>/dev/null; [ $? -eq 1 ]&& echo -e "*")
  echo " ❯ $FG_BRIGHT_GREEN"$branch$dirty
}

# Rename tab
function name () {
  echo -ne "\033]0;"$@"\007"
}

# Toggle theme background
function tbg () {
  if [ $ITERM_PROFILE = "dark" ]
  then
    export ITERM_PROFILE="light"
  else
    export ITERM_PROFILE="dark"
  fi

  echo -en "\033]50;SetProfile=${ITERM_PROFILE}\a"
}

# Prompt
export CLICOLOR=1
export PS1="\n\[$FG_BRIGHT_YELLOW\]\W\[\$(git_info)\] ❯ \[$RESET\]"

# Aliases
alias ls="ls -GFh"
alias reload="source ~/.bash_profile"
alias edit="name \${PWD##*/} edit && nvim"
alias ..="cd .."
alias ...="cd .. && cd -"
alias cdp="cd ~/Documents/Programming"
alias cdw="cd ~/Documents/Work/Projects"
alias neofetch="neofetch --image ascii"
alias gs="git status"
alias gitpurge='(git branch --merged | grep -Ev "(\*|master|develop|staging)" | xargs -n 1 git branch -d) && (git remote prune origin)'

# thefuck
eval $(thefuck --alias)

# Stop here if this is not an interactive shell (e.g. ssh <hostname> ls)
[[ $- == *i* ]] || return