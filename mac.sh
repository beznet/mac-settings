echo "Installing Mac..."

# App Store login
read -p "Login to App Store then press Enter to continue"

# Install xcode command line tools
echo "\nInstalling XCode command line tools..."
xcode-select --install
read -p "Install XCode command line tools then press Enter to continue"

# Install .bash_profile
echo "\nInstalling ~/.bash_profile..."
cp .bash_profile ~/.bash_profile

# Install .bashrc
echo "\nInstalling ~/.bashrc..."
cp .bashrc ~/.bashrc

# Source bash_profile
echo "\nSourcing ~/.bash_profile..."
source ~/.bash_profile

# Install nvm
echo "\nInstalling nvm..."
mkdir $NVM_DIR
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

# Install rvm
echo "\nInstalling rvm..."
curl -sSL https://get.rvm.io | bash -s stable

# Install pyenv
echo "\nInstalling pyenv..."
git clone https://github.com/pyenv/pyenv.git ~/.pyenv

# Install pyenv virtualenv
echo "\nInstalling pyenv-virtualenv..."
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv

# Install Homebrew
echo "\nInstalling Homebrew..."
if ! command -v brew >/dev/null; then
  curl -fsS 'https://raw.githubusercontent.com/Homebrew/install/master/install' | ruby

  export PATH="/usr/local/bin:$PATH"
fi

# Update brew
echo "\nUpdating Homebrew formulae..."
brew update --force

# Install/update a brew package
function brew_install {
  if brew ls --versions "$1" >/dev/null; then
    HOMEBREW_NO_AUTO_UPDATE=1 brew upgrade "$1"
  else
    HOMEBREW_NO_AUTO_UPDATE=1 brew install "$1"
  fi
}

# Brew taps
echo "\nTapping brews..."
brew tap caskroom/versions
brew tap caskroom/fonts

# Install brew packages
echo "\nInstalling brew packages..."
brew_install awsebcli
brew_install cocoapods
brew_install git
brew_install bash-completion
brew_install heroku/brew/heroku
brew_install imagemagick
brew_install mas
brew_install neofetch
brew_install neovim
brew_install openssl
brew_install readline
brew_install ripgrep
brew_install thefuck
brew_install wget
brew_install yarn

# Install brew cast packages
echo "\nInstalling brew cask packages..."
brew cask install alfred
brew cask install android-studio
brew cask install anylist
brew cask install bartender
brew cask install cleanmymac
brew cask install docker
brew cask install dropbox
brew cask install firefox
brew cask install font-hack
brew cask install font-hack-nerd-font
brew cask install font-hack-nerd-font-mono
brew cask install google-chrome
brew cask install insomnia
brew cask install iterm2
brew cask install java8
brew cask install keepingyouawake
brew cask install messenger
brew cask install mongodb-compass-community
brew cask install rocket
brew cask install sip
brew cask install sqlpro-for-mssql
brew cask install sqlpro-for-mysql
brew cask install sqlpro-for-postgres
brew cask install transmit
brew cask install tunnelblick
brew cask install vnc-server
brew cask install vnc-viewer

# Install Mac App Store apps
echo "\nInstalling Mac apps..."
mas lucky "Marked 2"
mas lucky "Microsoft Remote Desktop 10"
mas lucky "The Unarchiver"
mas lucky Cascadea
mas lucky Encrypto
mas lucky Keynote
mas lucky Moom
mas lucky Numbers
mas lucky Pages
mas lucky Pixelmator
mas lucky Slack
mas lucky Xcode
mas upgrade

# Source bash_profile
echo "\nSourcing ~/.bash_profile..."
source ~/.bash_profile

# Install node
echo "\nInstalling node..."
nvm install 10.14.1
nvm use 10.14.1
nvm alias default 10.14.1

# Install node packages
echo "\nInstalling neovim node..."
npm install -g neovim

# Install python
echo "\nInstalling python..."
pyenv install 2.7.11
pyenv install 3.6.1

# Setup python for neovim
echo "\nSetting up neovim python..."
pyenv virtualenv 2.7.11 neovim2
pyenv virtualenv 3.6.1 neovim3
pyenv activate neovim2
pip install neovim
pyenv activate neovim3
pip install neovim

# Install neovim settings
echo "\nInstalling neovim init.vim..."
mkdir ~/.config
mkdir ~/.config/nvim
wget -O ~/.config/nvim/init.vim https://gitlab.com/beznet/vim-settings/raw/master/init.vim

# Git settings
echo "\nSetting up git config..."
git config --global user.name "Bennett Dungan"
git config --global user.email "bennettdungan@gmail.com"
git config --global push.default simple
git config --global pull.rebase true

# Mac settings
echo "\nSetting up various Mac settings..."
defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false
echo '127.0.0.1 scp.localhost' | sudo tee -a /etc/hosts

# Manual installs
# Cisco AnyConnect Secure Mobility Client
#   Go to svpn.tsged.com on LTE
# Operator Mono
# Neovim App
# Keylord

# Manual settings
# Set iTerm2 Settings Folder to ~/Dropbox/iTerm
# Set Alfred setting to ~/Dropbox/Alfred
# System Preferences settings
# Safari settings
# Safari extensions
# Mail settings
# Notification center settings
# Moom settings
# SQLPro MSSQL settings
# SQLPro Postgres settings
# SQLPro MySQL settings
# Keylord settings
# MongoDB Compass settings
# Bartender settings
